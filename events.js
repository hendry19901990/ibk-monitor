var request = { 
	"jsonrpc": "2.0", 
	"method": "query", 
	"params": { 
	"type": 1, 
	"chaincodeID": { 
	"name": "095fb94438b9baa981095b175e7d93b20a3b7fdb91c825bf3cc7b85a5265427c3ad5cdde04cdac73216d6c9e9779fb050efc6404ef0223ebef8f366b39ff299d" 

	}, 
	"ctorMsg": { 
	"function": "getmovimientos", 
	"args": [ 
	"Movement"
	] 
	}, 
	"secureContext": "user_type1_1" 
	}, 
	"id": 0 
};

var endpoint = "https://94eb2dcb19fc4abca4fbb15bf9cfdbff-vp1.us.blockchain.ibm.com:5002/chaincode";

var loadingBarMust = true;
// create an array with edges
var edges = new vis.DataSet([]); 
var arrayEdges = new Array(); // Create because no exist method sort in network edges of vis.js
const imgDefault = 'img/profilepics/avatar.png';

var nodes = new vis.DataSet([]);

// create a network
var container = document.getElementById('mynetwork');

const sizeOneHoundredPercent = 200; // max height of image of Nodes
const MaxHeightBalance = 180;
const minHeightBalance = 30;
const defaultRender = 50;
//console.log(totalSupply);

var executeFunc = true;
var calculateBalances = false;
const maxWidth = 496;
var width = 20;

var data = {
 nodes: nodes,
 edges: edges
};

var parserOptions = {
  edges: {
    inheritColors: false
  },
  nodes: {
    fixed: true,
    parseColor: false
  }
};

var options = {
  autoResize: true,
  edges: {
    smooth: {
      type: "discrete"
    }
  },
  layout:{
	randomSeed:34
  },
  physics: {
    hierarchicalRepulsion: {
      nodeDistance: 135
    }, 
	forceAtlas2Based: {
		gravitationalConstant: -26,
		centralGravity: 0.005,
		springLength: 230,
		springConstant: 0.18
    },
	maxVelocity: 146,
	solver: 'forceAtlas2Based',
	timestep: 0.35,
	stabilization: {
		enabled:true,
		iterations:2000,
		updateInterval:25
	}
  }
};

setTimeout(barPercent, 100);
var network = new vis.Network(container, data, options);
requestData();


function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++ ) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function containsObject(id) {
    var list = nodes._data;
    for (var i in list) {
        if (list[i].id == id) {
            return true;
        }
    }
    return false;
}

function getIcons() {
     return {
          face: 'FontAwesome',
          code: '\uf007',
          
          color: getRandomColor()
		};
}

function getshapeProperties(){
	return {
		useImageSize: true
	};
}

function containsConexion(id) {
    var list = edges._data;
    var i;
    for (var i in list) {
        if (list[i].id == id)
            return true;
    }
    return false;
}

function getEdge(id) {

    var list = edges._data;
    var i;
    for (var i in list) {
        if (list[i].id == id)
            return list[i];
    }
    return null;

}

function userPicture(userid){    
	return "img/profilepics/" + userid + ".png";
}

function getSizeImgBalancefrom(userid){
  return userPicture(userid);
}

function getSizeImg(userid){
    var sizeImg;
    if (calculateBalances) {
	    var balanceFrom = contract.balanceOf.call(userid, {"from": web3.eth.accounts[0]}).c[0];
		sizeImg = (balanceFrom * sizeOneHoundredPercent)/totalSupply;
	} else {
		sizeImg = 50;
	}
   return sizeImg;
}

function getNodeProfile(id, id_string){
  var url = 'img/profilepics/' + id_string + '.png';
  return { id: id, shape: 'image', image: url, size: defaultRender, brokenImage: imgDefault, title: id_string};
}


function barPercent() {

    if(width > maxWidth)
		width = 20;
	
	width +=10;
	
	document.getElementById('bar').style.width = width + 'px';	   
	document.getElementById('text').innerHTML = parseInt(((width*100)/maxWidth)) + '%';
	
	if(executeFunc)
		setTimeout(barPercent, 100);
	
}

function updateSizeImg(nodeObj){
	var sizeImg = getSizeImg(nodeObj.userid);
	
	try{
   	  if (sizeImg > minHeightBalance){
		nodeObj.size = sizeImg;
		//console.log("Normal Height !!");
	  }else{
		nodeObj.size = minHeightBalance;
		//console.log("minHeightBalance !!");
	  }
	  nodes.update(nodeObj);
	}catch(err){
		console.log(err.message);
	}
}



/*
setTimeout(function(){ 
  if (!calculateBalances){
    var list = nodes._data;
    for (var i in list){
      updateSizeImg(list[i].id, list[i].title);
    }
    calculateBalances = true;
  }
 }, 30000);
*/

function getIdOFConnection(edgeActual){
	var i;
	for ( i in arrayEdges){
		if (arrayEdges[i].id == edgeActual.id && arrayEdges[i].block == edgeActual.block)
		return i;
	}
}



function requestData(){
   $.ajax({
     url: endpoint, 
     data: JSON.stringify(request),  // id is needed !!
     type: "POST",
     contentType: "application/json",
     dataType: "json",
     success:  function (data) { receiveData(data);},
     error: function (err)  { console.log (err);}
   });

   setTimeout(requestData, 1000);

}

function receiveData(data){

   if(data.result.status == "OK"){
	  if (loadingBarMust){
		loadingBarMust = false;
		document.getElementById('loadingBar').style.display = 'none';
	  }

	  var message = JSON.parse(data.result.message);
	  message.sort(function (ed_a, ed_b){return ed_a.time - ed_b.time;});
      for(var mov in message){

        try {
      	   if (message[mov].type == "W")
      	   	  continue;
           
           var walletId   = parseInt(message[mov].walletid);
           var businessId = message[mov].business;

           var isnum = /^\d+$/.test(businessId);

           if(isnum){
              businessId = parseInt(businessId);  
           }else{
           	  businessId = businessId.toLowerCase();
              businessId = businessId.trim();
              businessId = parseInt(toHex(businessId), 16);
           }
           
           var _from   = (message[mov].type == "D")? message[mov].walletid : message[mov].business;
	       var _to     = (message[mov].type == "D")? message[mov].business : message[mov].walletid;
           var _fromID = (message[mov].type == "D")? walletId   : businessId;
	       var _toID   = (message[mov].type == "D")? businessId : walletId;
	       var description = message[mov].amount;
	       var colorEdge   = getRandomColor(); 
	       var opacity = 0.3;

	       var fromObj = nodes.get(_fromID);
		   var toObj   = nodes.get(_toID);

		   if (fromObj == null){
               fromObj = getNodeProfile(_fromID, _from);
               nodes.add(fromObj);
		   }

		   if (toObj == null){
               toObj = getNodeProfile(_toID, _to); 
               nodes.add(toObj);
		   }

		   var idEdge = _fromID + _toID;
		   var arrows = "to";

		   if (containsConexion(idEdge)){

		   	   var edgeActual = getEdge(idEdge);
					 
				if (edgeActual.to == _fromID)
					arrows = "from";
					
				if (edgeActual.color.opacity < 1.0)
					edgeActual.color.opacity += 0.1;

				//console.log(description);
			
			    edgeActual.label = description; 
			    edges.update(edgeActual);

		   }else{
               var longitud = Math.floor((Math.random() * 370) + 300);
			   var newEdge = {id: idEdge, from: _fromID, to: _toID, arrows: arrows, label: description, length: longitud,  color: {color: colorEdge, highlight: colorEdge, opacity: opacity }};
			   edges.add(newEdge);
		   }

		}catch (err) {
			console.log(err.message);
		}

      }


   }else{
   	  alert ("Error in the Response!");
   }

}

function toHex(str) {
    var result = '';
    for (var i=0; i<str.length; i++) {
      result += str.charCodeAt(i).toString(16);
    }
    return parseInt(result);
}