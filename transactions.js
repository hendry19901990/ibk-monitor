var times = [];
var request = { 
    "jsonrpc": "2.0", 
    "method": "query", 
    "params": { 
    "type": 1, 
    "chaincodeID": { 
    "name": "095fb94438b9baa981095b175e7d93b20a3b7fdb91c825bf3cc7b85a5265427c3ad5cdde04cdac73216d6c9e9779fb050efc6404ef0223ebef8f366b39ff299d" 

    }, 
    "ctorMsg": { 
    "function": "getmovimientos", 
    "args": [ 
    "Movement"
    ] 
    }, 
    "secureContext": "user_type1_1" 
    }, 
    "id": 0 
};

var endpoint = "https://94eb2dcb19fc4abca4fbb15bf9cfdbff-vp1.us.blockchain.ibm.com:5002/chaincode";

function requestData(){
    console.log("Call");
   $.ajax({
     url: endpoint, 
     data: JSON.stringify(request),  // id is needed !!
     type: "POST",
     contentType: "application/json",
     dataType: "json",
     success:  function (data) { receiveData(data);},
     error: function (err)  { console.log (err);}
   });

   setTimeout(requestData, 10000);

}

function receiveData(data){
    if(data.result.status == "OK"){
        var message = JSON.parse(data.result.message);
        message.sort(function (a, b){return b.time - a.time;});
        for(var mov in message){

            var idTX = message[mov].time +  parseInt(message[mov].walletid);
            if(times.indexOf(idTX) != -1)
                continue;

            times.push(idTX);

            var trFinal = '<tr>';
            trFinal += "<td>"+message[mov].walletid+"</td>";
            trFinal += "<td>"+message[mov].business+"</td>";
            trFinal += "<td>"+message[mov].balance+"</td>";
            trFinal += "<td>"+message[mov].amount+"</td>";
            trFinal += "<td>"+message[mov].type+"</td>";
            trFinal += "<td>"+getTimeFormatted(message[mov].time)+"</td>";
            trFinal += "</tr>";
            $('#result-event tbody').append(trFinal);
        }
    }else{
       alert ("Error in the Response!");
    }
}

function getTimeFormatted(time){
    return new Date(time);
}

requestData();

