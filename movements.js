const NRequest = 4;
var chaincodeArr = [];

chaincodeArr["Inkafarma"]  = "422399831b9758864a7ae00060135295e0a96a58b66f0e4583568cfd61243b2ced745135bd7f78bacea04f6751928a70af44b439ec9f2681f1045ae27f2a8f0e";
chaincodeArr["Cineplanet"] = "85835cd4118478fb222612cf7f2f64edb42b4a5e99af08434c5795702d0ce953b12b713dd5464b09f0f53fb191e3a1e503d6bc9c9faf581bcd4f8f0b4afe633e";
chaincodeArr["Promart"]    = "d615a4942ac1253806eadfae2e0d4030d2bacc6a123a5741b30f7372b62c0b269bda5c55547be33a58e1ae9fc49fcebcdce4004434c36d38374264b579e5a37d";
chaincodeArr["Vivanda"]    = "36e077e17f40a3aadfabd6b312b4b1a064a22ef1bc4e2a69b787907ae1cdb0fbaa5fbae7e0a98dbea59b8211424e79814448c5da991a15b51391289deafb2dcd";

var endpoint = "https://94eb2dcb19fc4abca4fbb15bf9cfdbff-vp1.us.blockchain.ibm.com:5002/chaincode";

function drawBubble(root){

    d3.selectAll("svg > *").remove();

    var w = window.innerWidth*0.68*0.95;
    var h = Math.ceil(w*0.7);
    var oR = 0;
    var nTop = 0;

    var svgContainer = d3.select("#mainBubble")
      .style("height", h+"px");

    var svg = d3.select("#mainBubble").append("svg")
        .attr("class", "mainBubbleSVG")
        .attr("width", w)
        .attr("height",h)
        .on("mouseleave", function() {return resetBubbles();});
         
    var mainNote = svg.append("text")
    .attr("id", "bubbleItemNote")
    .attr("x", 10)
    .attr("y", w/2-15)
    .attr("font-size", 12)
    .attr("dominant-baseline", "middle")
    .attr("alignment-baseline", "middle")
    .style("fill", "#888888"); 

    var bubbleObj = svg.selectAll(".topBubble")
            .data(root.data)
        .enter().append("g")
            .attr("id", function(d,i) {return "topBubbleAndText_" + i});

    nTop = root.data.length;
    oR = w/(1+3*nTop);  

    h = Math.ceil(w/nTop*2);
    svgContainer.style("height",h+"px");
         
        var colVals = d3.scale.category10();
         
        bubbleObj.append("circle")
            .attr("class", "topBubble")
            .attr("id", function(d,i) {return "topBubble" + i;})
            .attr("r", function(d) { return oR; })
            .attr("cx", function(d, i) {return oR*(3*(1+i)-1);})
            .attr("cy", (h+oR)/3)
            .style("fill", function(d,i) { return colVals(i); }) // #1f77b4
        .style("opacity",0.3)
            .on("mouseover", function(d,i) {return activateBubble(d,i);});
         
             
        bubbleObj.append("text")
            .attr("class", "topBubbleText")
            .attr("x", function(d, i) {return oR*(3*(1+i)-1);})
            .attr("y", (h+oR)/3)
        .style("fill", function(d,i) { return colVals(i); }) // #1f77b4
            .attr("font-size", 30)
            .attr("text-anchor", "middle")
        .attr("dominant-baseline", "middle")
        .attr("alignment-baseline", "middle")
            .text(function(d) {return d.business})      
            .on("mouseover", function(d,i) {return activateBubble(d,i);});
         
         
        for(var iB = 0; iB < nTop; iB++)
        {

          var dataSet = ["balance: "+root.data[iB].balance, "spend: "+root.data[iB].spend, "sents: "+root.data[iB].sents];
          
            var childBubbles = svg.selectAll(".childBubble" + iB)
                .data(dataSet)
                .enter().append("g");
                 
        //var nSubBubble = Math.floor(root.children[iB].children.length/2.0);   
             
            childBubbles.append("circle")
                .attr("class", "childBubble" + iB)
                .attr("id", function(d,i) {return "childBubble_" + iB + "sub_" + i;})
                .attr("r",  function(d) {return oR/3.0;})
                .attr("cx", function(d,i) {return (oR*(3*(iB+1)-1) + oR*1.5*Math.cos((i-1)*45/180*3.1415926));})
                .attr("cy", function(d,i) {return ((h+oR)/3 +        oR*1.5*Math.sin((i-1)*45/180*3.1415926));})
                .attr("cursor","pointer")
                .style("opacity",0.5)
                .style("fill", "#eee")
            /*.on("mouseover", function(d,i) {
              //window.alert("say something");
              var noteText = "";
              if (d.note == null || d.note == "") {
                noteText = d;
              } else {
                noteText = d;
              }
              d3.select("#bubbleItemNote").text(noteText);
              })*/
            .append("svg:title")
            .text(function(d) { return d.balance; });   

            childBubbles.append("text")
                .attr("class", "childBubbleText" + iB)
                .attr("x", function(d,i) {return (oR*(3*(iB+1)-1) + oR*1.5*Math.cos((i-1)*45/180*3.1415926));})
                .attr("y", function(d,i) {return ((h+oR)/3 +        oR*1.5*Math.sin((i-1)*45/180*3.1415926));})
                .style("opacity",0.5)
                .attr("text-anchor", "middle")
            .style("fill", function(d,i) { return colVals(iB); }) // #1f77b4
                .attr("font-size", 6)
                .attr("cursor","pointer")
                .attr("dominant-baseline", "middle")
            .attr("alignment-baseline", "middle")
                .text(function(d) {return d}); 

        }

    resetBubbles = function () {
      w = window.innerWidth*0.68*0.95;
      oR = w/(1+3*nTop);
       
      h = Math.ceil(w/nTop*2);
      svgContainer.style("height",h+"px");

      mainNote.attr("y",h-15);
           
      svg.attr("width", w);
      svg.attr("height",h);       
       
     // d3.select("#bubbleItemNote").text("D3.js bubble menu developed by Shipeng Sun (sunsp.gis@gmail.com), Institute of Environment, University of Minnesota, and University of Springfield, Illinois.");
       
      var t = svg.transition()
          .duration(650);
         
        t.selectAll(".topBubble")
            .attr("r", function(d) { return oR; })
            .attr("cx", function(d, i) {return oR*(3*(1+i)-1);})
            .attr("cy", (h+oR)/3);

        t.selectAll(".topBubbleText")
        .attr("font-size", 30)
            .attr("x", function(d, i) {return oR*(3*(1+i)-1);})
            .attr("y", (h+oR)/3);
     
      for(var k = 0; k < nTop; k++) 
      {
        t.selectAll(".childBubbleText" + k)
                .attr("x", function(d,i) {return (oR*(3*(k+1)-1) + oR*1.5*Math.cos((i-1)*45/180*3.1415926));})
                .attr("y", function(d,i) {return ((h+oR)/3 +        oR*1.5*Math.sin((i-1)*45/180*3.1415926));})
            .attr("font-size", 6)
                .style("opacity",0.5);

        t.selectAll(".childBubble" + k)
                .attr("r",  function(d) {return oR/3.0;})
            .style("opacity",0.5)
                .attr("cx", function(d,i) {return (oR*(3*(k+1)-1) + oR*1.5*Math.cos((i-1)*45/180*3.1415926));})
                .attr("cy", function(d,i) {return ((h+oR)/3 +        oR*1.5*Math.sin((i-1)*45/180*3.1415926));});
                     
      }   
    }
         
         
    function activateBubble(d,i) {
            // increase this bubble and decrease others
            var t = svg.transition()
                .duration(d3.event.altKey ? 7500 : 350);
     
            t.selectAll(".topBubble")
                .attr("cx", function(d,ii){
                    if(i == ii) {
                        // Nothing to change
                        return oR*(3*(1+ii)-1) - 0.6*oR*(ii-1);
                    } else {
                        // Push away a little bit
                        if(ii < i){
                            // left side
                            return oR*0.6*(3*(1+ii)-1);
                        } else {
                            // right side
                            return oR*(nTop*3+1) - oR*0.6*(3*(nTop-ii)-1);
                        }
                    }               
                })
                .attr("r", function(d, ii) { 
                    if(i == ii)
                        return oR*1.8;
                    else
                        return oR*0.8;
                    });
                     
            t.selectAll(".topBubbleText")
                .attr("x", function(d,ii){
                    if(i == ii) {
                        // Nothing to change
                        return oR*(3*(1+ii)-1) - 0.6*oR*(ii-1);
                    } else {
                        // Push away a little bit
                        if(ii < i){
                            // left side
                            return oR*0.6*(3*(1+ii)-1);
                        } else {
                            // right side
                            return oR*(nTop*3+1) - oR*0.6*(3*(nTop-ii)-1);
                        }
                    }               
                })          
                .attr("font-size", function(d,ii){
                    if(i == ii)
                        return 30*1.5;
                    else
                        return 30*0.6;              
                });
     
            var signSide = -1;
            for(var k = 0; k < nTop; k++) 
            {
                signSide = 1;
                if(k < nTop/2) signSide = 1;
                t.selectAll(".childBubbleText" + k)
                    .attr("x", function(d,i) {return (oR*(3*(k+1)-1) - 0.6*oR*(k-1) + signSide*oR*2.5*Math.cos((i-1)*45/180*3.1415926));})
                    .attr("y", function(d,i) {return ((h+oR)/3 + signSide*oR*2.5*Math.sin((i-1)*45/180*3.1415926));})
                    .attr("font-size", function(){
                            return (k==i)?12:6;
                        })
                    .style("opacity",function(){
                            return (k==i)?1:0;
                        });
                     
                t.selectAll(".childBubble" + k)
                    .attr("cx", function(d,i) {return (oR*(3*(k+1)-1) - 0.6*oR*(k-1) + signSide*oR*2.5*Math.cos((i-1)*45/180*3.1415926));})
                    .attr("cy", function(d,i) {return ((h+oR)/3 + signSide*oR*2.5*Math.sin((i-1)*45/180*3.1415926));})
                    .attr("r", function(){
                            return (k==i)?(oR*0.55):(oR/3.0);               
                    })
                    .style("opacity", function(){
                            return (k==i)?1:0;                  
                        }); 
            }                   
     }
     
    window.onresize = resetBubbles;
 
}

function getParams (name) {
 return  { 
    jsonrpc: "2.0", 
    method: "query", 
    params: { 
        type: 1, 
        chaincodeID: { 
           name: name 
         }, 
        ctorMsg: { 
           function: "gettotalcoin", 
           args: [ 
            "coinBalance"
           ] 
        }, 
        secureContext: "user_type1_1" 
    }, 
    id: 0 
    };
}

function constructBusiness(nBusiness, respBusiness){
    if(respBusiness === null){
         return { business: nBusiness, balance: 0, spend: 0, sents: 0}
    }else{
         return { business: nBusiness, balance: respBusiness.balance, spend: respBusiness.spend, sents: respBusiness.sents} 
    }
}

function requestData(){
   console.log("Call");
   var rSents = 0;
   var businessArrRep = [];
   var dataFinal =  { totalcoins: "bubble", data: null };
   for(var n in chaincodeArr) {
       const n2 = n;
       var params = getParams(chaincodeArr[n]);
     
       $.ajax({
           url: endpoint, 
           data: JSON.stringify(params),  // id is needed !!
           type: "POST",
           contentType: "application/json",
           dataType: "json",
           success:  function (resp) { 

                    var b; 
                    try{
                        b = constructBusiness(n2, JSON.parse(resp.result.message));
                    }catch(err){
                        console.log(err);
                        b = constructBusiness(n2, null);
                    }

                    businessArrRep.push(b);
                    rSents++;
                    if (rSents == NRequest){
                        dataFinal.data = businessArrRep;
                        //console.log(dataFinal);
                        drawBubble(dataFinal);
                        setTimeout(requestData, 10000);
                    }
           },
           error: function (err)  { 
                    var b = constructBusiness(n2, null);
                    businessArrRep.push(b);
                    rSents++;
                    if (rSents == NRequest){
                        dataFinal.data = businessArrRep;
                        //console.log(dataFinal);
                        drawBubble(dataFinal);
                        setTimeout(requestData, 10000);
                    }
                    console.log (err);
            }
       });

   }

}

requestData();
getTotalCoins();

function getTotalCoins(){

    var parr = document.getElementById('totalcoins');
    var requestTotal = { 
        "jsonrpc": "2.0", 
        "method": "query", 
        "params": { 
        "type": 1, 
        "chaincodeID": { 
        "name": "095fb94438b9baa981095b175e7d93b20a3b7fdb91c825bf3cc7b85a5265427c3ad5cdde04cdac73216d6c9e9779fb050efc6404ef0223ebef8f366b39ff299d" 

        }, 
        "ctorMsg": { 
        "function": "gettotalcoin", 
        "args": [ 
          "coinBalance"
        ] 
        }, 
        "secureContext": "user_type1_1" 
        }, 
        "id": 0 
   };

  $.ajax({
     url: endpoint, 
     data: JSON.stringify(requestTotal),  // id is needed !!
     type: "POST",
     contentType: "application/json",
     dataType: "json",
     success:  function (resp) { 
           var t = 0;
           try{
                var dx = JSON.parse(resp.result.message);
                t = dx.response;
            }catch(err){
                t = 0;
            }  
            parr.innerHTML  = "<b><em>Intercoins</em> " + t + "</b>";   
      },
     error: function (err)  { console.log (err);}
   });

}